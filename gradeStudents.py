
import argparse
import os
import pandas as pd
import matlab.engine
import warnings
from canvasapi import Canvas
import tempfile

def studentInfo(fName):
    fnl = fName.split('_')
    try:
        cid = int(fnl[1])
    except ValueError:
        cid = int(fnl[2])
    return cid

def gradeAssignment(path, eng):
    try:
        # Try to run the script in matlab
        getattr(eng, path[:-2])(nargout=0)
    except matlab.engine.MatlabExecutionError:
        pass
    print(path)
    with open(path, 'r') as f:
        print(f.read())
    grade = None
    while grade is None:
        try:
            grade = int(input('Grade: '))
        except ValueError:
            continue
    feedback = ''
    while feedback == '':
        feedback = input('Feedback: ')
    return grade, feedback

def gradeLoop(eng, df, files):
    for f in files:
        try:
            cid = studentInfo(f)
        except IndexError:
            continue
        print('\n\n')
        eng.clear(nargout=0)
        eng.close(nargout=0)
        grade, feedback = gradeAssignment(f, eng)
        if cid not in df.index:
            df.loc[cid] = [grade, [feedback], [f]]
        else:
            prevRow = df.loc[cid]
            prevG = prevRow['Grade']
            prevFb = prevRow['Feedback']
            prevFiles = prevRow['Files']
            df.loc[cid] = [prevG + grade, prevFb + [feedback], prevFiles + [f]]

def makeDF():
    colDTypes = {
        'Grade': float,
        'Feedback': object,
        'Files': object,
    }
    try:
        df = pd.read_csv('grades.csv', dtype=colDTypes, index_col='CID')
        # Convert the Feedback and Files entries from strings into lists
        # Note that this is unsafe - using this on an untrusted csv file will get you into trouble
        df['Feedback'] = df['Feedback'].apply(eval)
        df['Files'] = df['Files'].apply(eval)
        
        seenFilesLL = df['Files']
        seenFiles = set()
        for l in seenFilesLL:
            for f in l:
                seenFiles.add(f)
    except FileNotFoundError:
        df = pd.DataFrame(columns = colDTypes.keys(), index=[])
        seenFiles = set()
        for c in colDTypes:
            df[c] = df[c].astype(colDTypes[c])
    return df, seenFiles

def fixBadNames():
    files = os.listdir()
    badStrings = ([f'-{v}.m' for v in range(1, 10)]
                  + [f' ({v}).m' for v in range(1, 10)])
    for fix in badStrings:
        newFiles = [f for f in files if f[-len(fix):] == fix]
        for f in newFiles:
            os.rename(f, f[:-len(fix)] + '.m')

def grader():
    df, seenFiles = makeDF()
    newFiles = [f for f in os.listdir()
                if f not in seenFiles and f[-2:] == '.m']
    newFiles = sorted(newFiles, key=lambda s: s[::-1])
    cancelled = False
    if len(newFiles) > 0:
        print(f'Grading {len(newFiles)} programs')
        try:
            with matlab.engine.start_matlab() as eng:
                gradeLoop(eng, df, newFiles)
        except:
            cancelled = True
        df.to_csv('grades.csv', index_label='CID')
    else:
        print('No new files to grade')
    print('All done :-)')
    return cancelled, df

def readCanvasInfo():
    with open('token.txt', 'r') as f:
        return f.readline(), int(f.readline())

def uploadGrades(api_key, course_id, df):
    # Code largely from Patrick Walls' canvas2mbgrader.ipynb script
    print('Uploading grades')
    API_URL = 'https://ubc.instructure.com'
    canvas = Canvas(API_URL, api_key)
    course = canvas.get_course(course_id)
    cont = 'n'
    while cont != 'y':
        assignmentID = int(input('Enter Canvas assignment ID: '))
        assignment = course.get_assignment(assignmentID)
        print(assignment.name)
        cont = input('Is this correct? (y/n): ')
    for scid in df.index:
        try:
            submission = assignment.get_submission(scid)
        except:
            print('Could not find assignment for {}'.format(scid))
            continue
        grade = df.loc[scid]['Grade']
        files = df.loc[scid]['Files']
        feedback = df.loc[scid]['Feedback']
        submission.edit(submission={'posted_grade': grade})
        with tempfile.NamedTemporaryFile('w+') as tf:
            tf.name = 'feedback.txt'
            for fn, fd in zip(files, feedback):
                tf.write(fn + ': ' + fd + '\n')
            tf.seek(0)
            submission.upload_comment(tf)

if __name__ == '__main__':
    warnings.filterwarnings('ignore')
    parser = argparse.ArgumentParser(description='Runs octave scripts and aggregates student grades')
    parser.add_argument('assignmentDir', type=str, help='The directory containing the assignments')
    parser.add_argument('-u', '--upload', action='store_true', help='Whether to upload the grades and feedback')
    args = parser.parse_args()
    if args.upload:
        api_key, course_id = readCanvasInfo()
    os.chdir(args.assignmentDir)
    fixBadNames()
    cancelled, df = grader()
    if not cancelled and args.upload:
        uploadGrades(api_key, course_id, df)
